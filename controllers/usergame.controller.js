const { UserGame } = require("../models");

class UserGameController {
  static async create(req, res, next) {
    try {
      await UserGame.create({
        name: req.body.name,
        price: req.body.price,
        publisher: req.body.publisher,
        genre: req.body.genre,
        userId: req.user.id,
      });

      res.status(201).json({
        message: "Successfully create new game user",
      });
    } catch (error) {
      next(error);
    }
  }

  static async list(req, res, next) {
    try {
      const data = await UserGame.findAll({
        where: {
          userId: req.user.id,
        },
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  }

  static async getById(req, res, next) {
    try {
      const data = await UserGame.findOne({
        where: {
          id: req.params.id,
        },
      });

      if (!data) {
        throw {
          status: 404,
          message: "User game data not found",
        };
      } else {
        if (data.userId !== req.user.id) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        } else {
          res.status(200).json(data);
        }
      }
    } catch (error) {
      next(error);
    }
  }

  static async updateData(req, res, next) {
    try {
      const data = await UserGame.findOne({
        where: {
          id: req.params.id,
        },
      });

      if (!data) {
        throw {
          status: 404,
          message: "User game data not found",
        };
      } else {
        await UserGame.update(req.body, {
          where: {
            id: req.params.id,
            userId: req.user.id,
          },
        });
        res.status(200).json({
          message: "Successfully update data user game",
        });
      }
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const data = await UserGame.findOne({
        where: {
          id: req.params.id,
        },
      });
      if (!data) {
        throw {
          status: 404,
          message: "Data not found",
        };
      } else {
        if (data.userId !== req.user.id) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        } else {
          await UserGame.destroy({
            where: {
              id: req.params.id,
            },
          });

          res.status(200).json({ message: "Successfully delete data" });
        }
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = UserGameController;
