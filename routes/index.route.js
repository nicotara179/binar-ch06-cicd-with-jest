const express = require("express");
const router = express.Router();

const authRoutes = require("./auth.route");
const userGameRoutes = require("./usergame.route");

router.use("/user", authRoutes);
router.use("/user-games", userGameRoutes);

module.exports = router;
