const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { route } = require("../app");

const UserGameController = require("../controllers/usergame.controller");

router.post(
  "/",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);
        if (user) {
          req.user = user;
          next();
        } else {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
      }
    } catch (error) {
      next(error);
    }
  },
  (req, res, next) => {
    const errors = [];
    if (!req.body.name) {
      errors.push("Name required");
    }
    if (!req.body.price) {
      errors.push("Price required");
    }
    if (!req.body.publisher) {
      errors.push("Publisher required");
    }
    if (!req.body.genre) {
      errors.push("Genre required");
    }

    if (errors.length > 0) {
      next({
        status: 400,
        message: errors,
      });
    } else {
      next();
    }
  },
  UserGameController.create
);

router.put(
  "/:id",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);
        if (user) {
          req.user = user;
          next();
        } else {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
      }
    } catch (error) {
      next(error);
    }
  },
  (req, res, next) => {
    const errors = [];
    if (req.body.name !== undefined) {
      if (!req.body.name) {
        errors.push("Name required");
      }
    }
    if (req.body.price !== undefined) {
      if (!req.body.price) {
        errors.push("Price required");
      }
    }
    if (req.body.publisher !== undefined) {
      if (!req.body.publisher) {
        errors.push("Publisher required");
      }
    }
    if (req.body.genre !== undefined) {
      if (!req.body.genre) {
        errors.push("Genre required");
      }
    }

    if (errors.length > 0) {
      next({
        status: 400,
        message: errors,
      });
    } else {
      next();
    }
  },
  UserGameController.updateData
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);
        if (user) {
          req.user = user;
          next();
        } else {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
      }
    } catch (error) {
      next(error);
    }
  },
  UserGameController.delete
);

router.get(
  "/",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);
        if (user) {
          req.user = user;
          next();
        } else {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
      }
    } catch (error) {
      next(error);
    }
  },
  UserGameController.list
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);

        if (user) {
          req.user = user;
          next();
        } else {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
      }
    } catch (error) {
      next(error);
    }
  },
  UserGameController.getById
);

module.exports = router;
