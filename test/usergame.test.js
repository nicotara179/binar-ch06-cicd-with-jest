const request = require("supertest");

const bcrypt = require("bcryptjs");

const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;

const app = require("../app");

const jwt = require("jsonwebtoken");

let token;
let token2;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("password123", salt);

  await queryInterface.bulkInsert("Users", [
    {
      email: "test@gmail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      email: "test2@gmail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);

  await queryInterface.bulkInsert("UserGames", [
    {
      userId: 1,
      name: "Game Puzzle",
      price: 50000,
      publisher: "Gamelofe",
      genre: "kids",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);

  token = jwt.sign(
    {
      id: 1,
      email: "test@gmail.com",
    },
    "secret"
  );

  token2 = jwt.sign(
    {
      id: 2,
      email: "test2@gmail.com",
    },
    "secret"
  );
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "Users",
    {},
    { truncate: true, restartIdentity: true }
  );
  await queryInterface.bulkDelete(
    "UserGames",
    {},
    { truncate: true, restartIdentity: true }
  );
});

describe("POST UserGame", () => {
  it("Success", (done) => {
    request(app)
      .post("/user-games")
      .set("authorization", token)
      .send({
        name: "Game Assasin Crid",
        price: 100000,
        publisher: "Unisoft",
        genre: "adult",
        userId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Successfully create new game user");
          done();
        }
      });
  });

  it("No Authorization", (done) => {
    request(app)
      .post("/user-games")
      .send({
        name: "Game Assasin Crid",
        price: 100000,
        publisher: "Unisoft",
        genre: "adult",
        userId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Auth Token", (done) => {
    request(app)
      .post("/user-games")
      .set("authorization", "slebeww")
      .send({
        name: "Game Assasin Crid",
        price: 100000,
        publisher: "Unisoft",
        genre: "adult",
        userId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Required field", (done) => {
    request(app)
      .post("/user-games")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message.length).toBe(4);
          expect(res.body.message.includes("Name required")).toBe(true);
          expect(res.body.message.includes("Price required")).toBe(true);
          expect(res.body.message.includes("Publisher required")).toBe(true);
          expect(res.body.message.includes("Genre required")).toBe(true);
          done();
        }
      });
  });
});

describe("GET All UserGame Data", () => {
  it("Success", (done) => {
    request(app)
      .get("/user-games")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(Array.isArray(res.body)).toBe(true);
          done();
        }
      });
  });

  it("No Authorization", (done) => {
    request(app)
      .get("/user-games")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Token", (done) => {
    request(app)
      .get("/user-games")
      .set("authorization", "tokensalah")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
});

describe("GET UserGame Data by ID", () => {
  it("Success", (done) => {
    request(app)
      .get("/user-games/1")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("name");
          expect(res.body).toHaveProperty("price");
          expect(res.body).toHaveProperty("publisher");
          expect(res.body).toHaveProperty("genre");
          expect(res.body.name).toBe("Game Puzzle");
          expect(res.body.price).toBe(50000);
          expect(res.body.publisher).toBe("Gamelofe");
          expect(res.body.genre).toBe("kids");
          done();
        }
      });
  });

  it("No Authorization", (done) => {
    request(app)
      .get("/user-games/1")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Token", (done) => {
    request(app)
      .get("/user-games/1")
      .set("authorization", "tokensalah")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Access by another user", (done) => {
    request(app)
      .get("/user-games/1")
      .set("authorization", token2)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Not Found", (done) => {
    request(app)
      .get("/user-games/99999")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("User game data not found");
          done();
        }
      });
  });
});

describe("UPDATE UserGame data by ID", () => {
  it("Success", (done) => {
    request(app)
      .put("/user-games/1")
      .set("authorization", token)
      .send({
        name: "Update name",
        price: 20000,
        publisher: "Update publisher",
        genre: "Update genre",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Successfully update data user game");
          done();
        }
      });
  });

  it("No Authorization", (done) => {
    request(app)
      .put("/user-games/1")
      .send({
        name: "Update name",
        price: 20000,
        publisher: "Update publisher",
        genre: "Update genre",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Token", (done) => {
    request(app)
      .put("/user-games/1")
      .set("authorization", "tokensalah")
      .send({
        name: "Update name",
        price: 20000,
        publisher: "Update publisher",
        genre: "Update genre",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Required field", (done) => {
    request(app)
      .put("/user-games/1")
      .set("authorization", token)
      .send({
        name: "",
        price: null,
        publisher: "",
        genre: "",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message.length).toBe(4);
          expect(res.body.message.includes("Name required")).toBe(true);
          expect(res.body.message.includes("Price required")).toBe(true);
          expect(res.body.message.includes("Publisher required")).toBe(true);
          expect(res.body.message.includes("Genre required")).toBe(true);
          done();
        }
      });
  });

  it("Not Found", (done) => {
    request(app)
      .get("/user-games/99999")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("User game data not found");
          done();
        }
      });
  });
});

describe("DELETE Data UserGame by ID", () => {
  it("Success", (done) => {
    request(app)
      .delete("/user-games/1")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Successfully delete data");
          done();
        }
      });
  });

  it("No Authorization", (done) => {
    request(app)
      .delete("/user-games/1")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Token", (done) => {
    request(app)
      .delete("/user-games/1")
      .set("authorization", "tokensalah")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Delete by another user", (done) => {
    request(app)
      .delete("/user-games/1")
      .set("authorization", token2)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Not Found", (done) => {
    request(app)
      .get("/user-games/99999")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("User game data not found");
          done();
        }
      });
  });
});
