const request = require("supertest");

const bcrypt = require("bcryptjs");

const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;

const app = require("../app");

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("password123", salt);

  await queryInterface.bulkInsert("Users", [
    {
      email: "test@gmail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "Users",
    {},
    { truncate: true, restartIdentity: true }
  );
});

describe("Login API Test", () => {
  it("Success", (done) => {
    request(app)
      .post("/user/login")
      .send({
        email: "test@gmail.com",
        password: "password123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("token");
          done();
        }
      });
  });

  it("Wrong password", (done) => {
    request(app)
      .post("/user/login")
      .send({
        email: "test@gmail.com",
        password: "salah",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Invalid email or password");
          done();
        }
      });
  });

  it("Wrong email", (done) => {
    request(app)
      .post("/user/login")
      .send({
        email: "salah@gmail.com",
        password: "password123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Invalid email or password");
          done();
        }
      });
  });
});
